export const data = [{
    "LeadSeqNo":668,
    "TerritoryNo":1,
    "ContactName":"Sample2 Phillips",
    "BusinessName":"Terrace Homes",
    "State":"SA",
    "PostCode":"",
    "Email":"james@lsconstruct.com.au ",
    "LeadSource":"Website",
    "InitialContact":"2020-01-23T00:00:00",
    "Meeting":null,
    "InfoSession":null,
    "ApplicationFormSent":null,
    "ApplicationFormReceived":null,
    "PreliminaryApprovalGranted":null,
    "FinalInterview":null,
    "FranchiseeAgreementSent":null,
    "Granted":null,
    "FollowUp":"2020-01-26T00:00:00",
    "Progress":"Meeting",
    "AreaOfInterest":"Adelaide Northern",
    "LeadStatus":"Current",
    "Consultant":"David Martin"
 },
 {
    "LeadSeqNo":660,
    "TerritoryNo":2,
    "ContactName":"Sample Phillips",
    "BusinessName":"Terrace Homes",
    "State":"SA",
    "PostCode":"",
    "Email":"james@lsconstruct.com.au ",
    "LeadSource":"Website",
    "InitialContact":"2020-01-23T00:00:00",
    "Meeting":null,
    "InfoSession":null,
    "ApplicationFormSent":null,
    "ApplicationFormReceived":null,
    "PreliminaryApprovalGranted":null,
    "FinalInterview":null,
    "FranchiseeAgreementSent":null,
    "Granted":null,
    "FollowUp":"2020-01-26T00:00:00",
    "Progress":"Meeting",
    "AreaOfInterest":"Adelaide Northern",
    "LeadStatus":"Current",
    "Consultant":"David Martin"
 },
 {
    "LeadSeqNo":661,
    "TerritoryNo":3,
    "ContactName":"Dummy Phillips",
    "BusinessName":"Terrace Homes",
    "State":"SA",
    "PostCode":"",
    "Email":"james@lsconstruct.com.au ",
    "LeadSource":"Website",
    "InitialContact":"2020-01-23T00:00:00",
    "Meeting":null,
    "InfoSession":null,
    "ApplicationFormSent":null,
    "ApplicationFormReceived":null,
    "PreliminaryApprovalGranted":null,
    "FinalInterview":null,
    "FranchiseeAgreementSent":null,
    "Granted":null,
    "FollowUp":"2020-01-26T00:00:00",
    "Progress":"Meeting",
    "AreaOfInterest":"Adelaide Northern",
    "LeadStatus":"Current",
    "Consultant":"David Martin"
 },
 {
    "LeadSeqNo":662,
    "TerritoryNo":4,
    "ContactName":"Dummy Phillips",
    "BusinessName":"Terrace Homes",
    "State":"SA",
    "PostCode":"",
    "Email":"james@lsconstruct.com.au ",
    "LeadSource":"Website",
    "InitialContact":"2020-01-23T00:00:00",
    "Meeting":null,
    "InfoSession":null,
    "ApplicationFormSent":null,
    "ApplicationFormReceived":null,
    "PreliminaryApprovalGranted":null,
    "FinalInterview":null,
    "FranchiseeAgreementSent":null,
    "Granted":null,
    "FollowUp":"2020-01-26T00:00:00",
    "Progress":"Meeting",
    "AreaOfInterest":"Adelaide Northern",
    "LeadStatus":"Current",
    "Consultant":"David Martin"
 },
 {
    "LeadSeqNo":663,
    "TerritoryNo":5,
    "ContactName":"Dummy Phillips",
    "BusinessName":"Terrace Homes",
    "State":"SA",
    "PostCode":"",
    "Email":"james@lsconstruct.com.au ",
    "LeadSource":"Website",
    "InitialContact":"2020-01-23T00:00:00",
    "Meeting":null,
    "InfoSession":null,
    "ApplicationFormSent":null,
    "ApplicationFormReceived":null,
    "PreliminaryApprovalGranted":null,
    "FinalInterview":null,
    "FranchiseeAgreementSent":null,
    "Granted":null,
    "FollowUp":"2020-01-26T00:00:00",
    "Progress":"Meeting",
    "AreaOfInterest":"Adelaide Northern",
    "LeadStatus":"Current",
    "Consultant":"David Martin"
 },
 {
    "LeadSeqNo":664,
    "TerritoryNo":6,
    "ContactName":"James Phillips",
    "BusinessName":"Terrace Homes",
    "State":"SA",
    "PostCode":"",
    "Email":"james@lsconstruct.com.au ",
    "LeadSource":"Website",
    "InitialContact":"2020-01-23T00:00:00",
    "Meeting":null,
    "InfoSession":null,
    "ApplicationFormSent":null,
    "ApplicationFormReceived":null,
    "PreliminaryApprovalGranted":null,
    "FinalInterview":null,
    "FranchiseeAgreementSent":null,
    "Granted":null,
    "FollowUp":"2020-01-26T00:00:00",
    "Progress":"Meeting",
    "AreaOfInterest":"Adelaide Northern",
    "LeadStatus":"Current",
    "Consultant":"David Martin"
 },
 {
    "LeadSeqNo":665,
    "TerritoryNo":7,
    "ContactName":"James Phillips",
    "BusinessName":"Terrace Homes",
    "State":"SA",
    "PostCode":"",
    "Email":"james@lsconstruct.com.au ",
    "LeadSource":"Website",
    "InitialContact":"2020-01-23T00:00:00",
    "Meeting":null,
    "InfoSession":null,
    "ApplicationFormSent":null,
    "ApplicationFormReceived":null,
    "PreliminaryApprovalGranted":null,
    "FinalInterview":null,
    "FranchiseeAgreementSent":null,
    "Granted":null,
    "FollowUp":"2020-01-26T00:00:00",
    "Progress":"Meeting",
    "AreaOfInterest":"Adelaide Northern",
    "LeadStatus":"Current",
    "Consultant":"David Martin"
 },
 {
    "LeadSeqNo":666,
    "TerritoryNo":8,
    "ContactName":"James Phillips",
    "BusinessName":"Terrace Homes",
    "State":"SA",
    "PostCode":"",
    "Email":"james@lsconstruct.com.au ",
    "LeadSource":"Website",
    "InitialContact":"2020-01-23T00:00:00",
    "Meeting":null,
    "InfoSession":null,
    "ApplicationFormSent":null,
    "ApplicationFormReceived":null,
    "PreliminaryApprovalGranted":null,
    "FinalInterview":null,
    "FranchiseeAgreementSent":null,
    "Granted":null,
    "FollowUp":"2020-01-26T00:00:00",
    "Progress":"Meeting",
    "AreaOfInterest":"Adelaide Northern",
    "LeadStatus":"Current",
    "Consultant":"David Martin"
 },
 {
    "LeadSeqNo":649,
    "TerritoryNo":9,
    "ContactName":"Emmanuel Hatzicosmas",
    "BusinessName":"Emmanuel 7 Construction",
    "State":"SA",
    "PostCode":"",
    "Email":"eh-7@bigpond.net.au ",
    "LeadSource":"Website",
    "InitialContact":null,
    "Meeting":null,
    "InfoSession":null,
    "ApplicationFormSent":null,
    "ApplicationFormReceived":null,
    "PreliminaryApprovalGranted":null,
    "FinalInterview":null,
    "FranchiseeAgreementSent":null,
    "Granted":null,
    "FollowUp":"2014-01-08T00:00:00",
    "Progress":"Application Sent",
    "AreaOfInterest":"Adelaide Metro and Surrounds",
    "LeadStatus":"Cold",
    "Consultant":"Paul Cabelli"
 },
 {
    "LeadSeqNo":650,
    "TerritoryNo":10,
    "ContactName":"Robert Stell",
    "BusinessName":"First National Gawler",
    "State":"SA",
    "PostCode":null,
    "Email":"0401 551 065",
    "LeadSource":"Website",
    "InitialContact":null,
    "Meeting":null,
    "InfoSession":null,
    "ApplicationFormSent":null,
    "ApplicationFormReceived":null,
    "PreliminaryApprovalGranted":null,
    "FinalInterview":null,
    "FranchiseeAgreementSent":null,
    "Granted":null,
    "FollowUp":"2011-06-06T00:00:00",
    "Progress":"Deposit",
    "AreaOfInterest":"Gawler",
    "LeadStatus":"Do Not Contact",
    "Consultant":"David Martin"
 },
 {
    "LeadSeqNo":651,
    "TerritoryNo":11,
    "ContactName":"Ray Gauci",
    "BusinessName":"Clearvision Developments",
    "State":"SA",
    "PostCode":"",
    "Email":"clearvision@bordernet.com.au ",
    "LeadSource":"Website",
    "InitialContact":null,
    "Meeting":null,
    "InfoSession":null,
    "ApplicationFormSent":null,
    "ApplicationFormReceived":null,
    "PreliminaryApprovalGranted":null,
    "FinalInterview":null,
    "FranchiseeAgreementSent":null,
    "Granted":null,
    "FollowUp":"2011-07-15T00:00:00",
    "Progress":"Deposit",
    "AreaOfInterest":"Gawler",
    "LeadStatus":"Do Not Contact",
    "Consultant":"David Martin"
 }];