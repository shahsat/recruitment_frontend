import {UPDATE_HEADERS} from "../Actions/type";


const initialState = {
    headers: [
        {
            key: 101,
            Type: "Key Dates",
            checked: false,
            innerHeader: [
                { key: 1, Header: "LeadSeqNo", checked: true },
                { key: 2, Header: "TerritoryNo", checked: true },
                { key: 3, Header: "ContactName", checked: true },
                { key: 4, Header: "BusinessName", checked: false },
                { key: 5, Header: "State", checked: false },
                { key: 6, Header: "PostCode", checked: false },
                { key: 7, Header: "Email", checked: false },
                { key: 8, Header: "LeadSource", checked: false },
                { key: 9, Header: "InitialContact", checked: false }
            ]
        },
        {
            key: 102,
            Type: "Terrritory",
            checked: false,
            innerHeader: [
                { key: 15, Header: "FinalInterview", checked: false },
                { key: 16, Header: "FranchiseeAgreementSent", checked: true },
                { key: 17, Header: "Granted", checked: false },
                { key: 18, Header: "FollowUp", checked: true },
                { key: 19, Header: "Progress", checked: false },
                { key: 20, Header: "AreaOfInterest", checked: true },
                { key: 21, Header: "LeadStatus", checked: false },
                { key: 22, Header: "Consultant", checked: true }
            ]
        },
        {
            key: 103,
            Type: "Lead Info",
            checked: false,
            innerHeader: [
                { key: 10, Header: "Meeting", checked: true },
                { key: 11, Header: "InfoSession", checked: false },
                { key: 12, Header: "ApplicationFormSent", checked: true },
                { key: 13, Header: "ApplicationFormReceived", checked: false },
                { key: 14, Header: "PreliminaryApprovalGranted", checked: true }
            ]
        }
    ]
};

export default function appReducers(state = initialState, action) {
    switch (action.type) {
        case UPDATE_HEADERS:
            // console.log(action.payload, "This are all current state")
            return {
                ...state, headers: action.payload,

            }

        default: {

            return state;
        }
    }
}
