import logo from './logo.svg';
import './App.css';
import React, { useEffect, useState } from 'react'
import RecruitmentGridAll from './containers/Recruitment/RecruitmentDetails/RecruitmentGridAll';
import { useSelector } from 'react-redux';
import RecruitmentPage from './containers/Recruitment/RecruitmentDetails/RecruitmentPage';


const App = () => {
  // const headers = useSelector((state) => state.headers);
  // console.log(headers,"form App.js")
  // let initialHeaders = headers.map((header) => header.innerHeader.filter((head) => head.checked))
  //   .flat();
  // const [checkedHeader, setCheckedHeader] = useState(initialHeaders);

  // useEffect(() => {
  //   setCheckedHeader(
  //     headers.map((header) => header.innerHeader.filter((head) => head.checked))
  //       .flat()
  //   );
  // }, [headers]);
  // console.log(headers, "header form reducers");

  // useEffect(() => { }, [headers]);
  return (
    <div className="App">
      <RecruitmentPage />
    </div>
  );
}

export default App;
