import React from 'react';
import Hoc from '../../../hoc/Hoc';
import {
    Divider,
    Header,
    Label,
    List,
    Table,
    Grid,
    Segment,
    Container,
    Button,
    Card,
  } from "semantic-ui-react";

const ViewRecruitment = (props) => {
    return(
        <Hoc>
           <React.Fragment>
               <Container textAlign="center">
                  <Button secondary href="/">Back to List</Button>
               </Container>
           </React.Fragment>
        </Hoc>
    )
}

export default ViewRecruitment;
