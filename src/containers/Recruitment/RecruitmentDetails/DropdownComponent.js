import React, { useState } from "react";
import { Button, Dropdown } from "semantic-ui-react";
import CheckboxComponent from "./CheckBox";
import DropdownModal from "./DropdownModal";

const DropdownComponent = (props) => {

  return (
    <Button color='blue'>
      <Dropdown text='Dropdown' simple >
        <Dropdown.Menu style={{ width: "700px", padding: "2rem" }}>
          <DropdownModal headers={props.headers} />
        </Dropdown.Menu>

      </Dropdown>
    </Button>


  )
}

export default DropdownComponent;
