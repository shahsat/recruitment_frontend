import React, { useState, useEffect } from "react";
import { Icon, Menu, Table, Header, Pagination, TableHeader, TableBody } from "semantic-ui-react";
import _ from "lodash";
import moment from "moment";
import { data } from '../../../Data/RecruitmentList';
import { useSelector } from "react-redux";

const HeaderParser = (items, data) => {
    return items.map((abc) =>
        Object.keys(abc)
            .filter((key) => data.includes(key))
            .reduce((obj, key) => {
                obj[key] = abc[key];
                return obj;
            }, {})
    );
};

let filteredList;

const RecruitmentGridAll = (props) => {
    const headers = props.headers;
    const [begin, setbegin] = useState(0);
    const [end, setend] = useState(5);
    const [items, setItems] = useState(data)
    const [sortedFilterList, setSortedFilterList] = useState(data);
    const [iActivePage, setiActivePage] = useState(1);
    const [column, setcolumn] = useState(null);
    const [direction, setdirection] = useState("ascending");
    const [checkedHeader, setCheckedHeader] = useState([]);
    const [tableHeader, settableHeader] = useState([]);
    const [tableBody, settableBody] = useState([])
    useEffect(() => {
        filteredList = items;
        if (filteredList !== undefined && filteredList.length > 0) {
            setSortedFilterList(filteredList.slice(begin, end))
        }

    }, [])

    useEffect(() => {
        filteredList = items;
        if (filteredList !== undefined && filteredList.length > 0) {
            setSortedFilterList(filteredList.slice(begin, end));
        }

        setCheckedHeader((checkedHeader) => headers.map(header => (header.checked) ? header.Header : ""))
    }, [headers, items]);

    useEffect(() => {
        console.log("Sorted Filtered List ", sortedFilterList, " Items ", items)
        let listWithFilteredHeader = HeaderParser(sortedFilterList, checkedHeader);
        let filteredHeader = Object.keys(listWithFilteredHeader[0]).map(key => key);
        settableHeader(filteredHeader.map(header => <Table.HeaderCell
            sorted={column === header ? direction : null}
            onClick={handleSort(header)}
        >
            {header}
        </Table.HeaderCell>))

        settableBody(listWithFilteredHeader.map(item => (
            <Table.Row key={item.LeadSeqNo}>
                {Object.keys(item).map(key => (<Table.Cell>{item[key]}</Table.Cell>))}
            </Table.Row>
        )))
    }, [checkedHeader, sortedFilterList])

    function handlePageChange(e, { activePage }) {
        setiActivePage(iActivePage);
        setbegin(activePage * 5 - 5);
        setend(activePage * 5);

        filteredList = _.sortBy(filteredList, [column]);
        if (direction === "descending") filteredList = filteredList.reverse();
        setSortedFilterList(filteredList.slice(activePage * 5 - 5, activePage * 5));
    }

    const handleSort = (clickedColumn) => () => {
        if (column !== clickedColumn) {
            console.log("Clicked Column name if", clickedColumn)

            setcolumn(clickedColumn);
            filteredList = _.sortBy(filteredList, [clickedColumn]);
            setdirection("ascending");
        } else {
            console.log("Clicked Column name else", clickedColumn)

            filteredList = filteredList.reverse();
            setdirection(direction === "ascending" ? "descending" : "ascending");
        }
        setSortedFilterList(filteredList.slice(begin, end));
    };
    function dateFormatter(date) {
        return date != null ? moment(date).format("DD-MM-YYYY") : ""; // new Date(data).toLocaleString('en-AU') : '';
    }

    return (
        <div style={{ width: "100%" }}>
            {/* <div style={{ maxwidth: "100%", overflow: "auto", gridAutoFlow: "scroll" }}> */}
            {sortedFilterList === undefined || sortedFilterList.length === 0 ? (
                <Header>No active job registration found</Header>
            ) : (
                <div>
                    <div style={{ width: "100%", overflow: "auto" }}>

                        <Table selectable celled sortable padded >
                            <Table.Header >
                                <Table.Row>
                                    {tableHeader}
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                {tableBody}
                            </Table.Body>
                            

                        </Table>
                    </div>
                    <div>

                        <Table>
                            <Table.Footer>
                                <Table.Row>
                                    <Table.HeaderCell colSpan={tableHeader.length}>
                                        <Menu floated="right" pagination>
                                            <Pagination
                                                defaultActivePage={1}
                                                totalPages={5}//{Math.ceil(filteredList.length / 5)}
                                                onPageChange={handlePageChange}
                                            />
                                        </Menu>
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>

                        </Table>
                    </div>

                </div>)}
        </div>
    )
}

export default RecruitmentGridAll;