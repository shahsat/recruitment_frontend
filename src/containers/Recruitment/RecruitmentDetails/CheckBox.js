import React, { useState, useEffect, useSelector } from "react";
import { Checkbox } from "semantic-ui-react";
import { useDispatch } from "react-redux";

const CheckboxComponent = ({ headers }) => {
    console.log(headers,"fromm checkbox");
    const dispatch = useDispatch();
    const [checkedItems, setCheckedItems] = useState(headers);

    const handleChange = (e, { value }) => {
        console.log(value, "checkbox clicked")
        setCheckedItems((checkItems) => checkItems.map((item) => item.Header === value ? { ...item, checked: !item.checked } : item))
    }

    useEffect(() => {
        dispatch({ type: "UPDATE_HEADERS", payload: checkedItems })
    }, [checkedItems]);

    return (
        <div>
            {checkedItems != undefined ? (checkedItems.map((item) => (
                <Checkbox
                    key={item.key}
                    label={item.Header}
                    value={item.Header}
                    onChange={handleChange}
                    checked={item.checked}
                />
            )
            )) : (<p>No Items</p>)
            }
        </div>
    )

}

export default CheckboxComponent;
