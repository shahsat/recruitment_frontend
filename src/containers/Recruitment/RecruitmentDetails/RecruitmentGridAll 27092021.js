import React, { useState, useEffect } from "react";
import { Icon, Menu, Table, Header, Pagination, TableHeader, TableBody } from "semantic-ui-react";
import _ from "lodash";
import moment from "moment";
//import { data } from '../../../Data/RecruitmentList';

const HeaderParser = (items, data) => {
    return items.map((abc) =>
        Object.keys(abc)
            .filter((key) => data.includes(key))
            .reduce((obj, key) => {
                obj[key] = abc[key];
                return obj;
            }, {})
    );
};


const RecruitmentGridAll = (props) => {
  //  let filteredList=[];
    const headers = props.headers;
    const [begin, setbegin] = useState(0);
    const [end, setend] = useState(5);
    // const [items, setItems] = useState(data)
    const items =props.items;
    const [filteredList,setFilteredList]=useState(items);
    const [sortedFilterList, setSortedFilterList] = useState(items);
    const [iActivePage, setiActivePage] = useState(1);
    const [column, setcolumn] = useState(null);
    const [direction, setdirection] = useState("ascending");
    const [checkedHeader, setCheckedHeader] = useState([]);
    const [tableHeader, settableHeader] = useState([]);
    const [tableBody, settableBody] = useState([])

    useEffect(() => {
        //filteredList = items;
        console.log(" items in begining ",items)
        console.log(" Filter list length ",filteredList.length,"  hello ",Math.ceil(filteredList.length / 5));
        if (filteredList !== undefined && filteredList.length > 0) {
            setSortedFilterList(filteredList.slice(begin, end))
        }

    }, [])

    useEffect(() => {
       // filteredList = items;
       console.log("When items changed ",items)
       setFilteredList(items)
        if (filteredList !== undefined && filteredList.length > 0) {
            setSortedFilterList(filteredList.slice(begin, end));
        }

        setCheckedHeader((checkedHeader) => headers.map(header => (header.checked) ? header.Header : ""))
    }, [headers, items]);

    useEffect(() => {
        console.log("Sorted Filtered List ", sortedFilterList, " Items ", items)
        let listWithFilteredHeader = HeaderParser(sortedFilterList, checkedHeader);
        let filteredHeader = Object.keys(listWithFilteredHeader[0]).map(key => key);
        settableHeader(filteredHeader.map(header => <Table.HeaderCell
            sorted={column === header ? direction : null}
            onClick={handleSort(header)}
        >
            {header}
        </Table.HeaderCell>))

        settableBody(listWithFilteredHeader.map(item => (
            <Table.Row key={item.LeadSeqNo}>
                {Object.keys(item).map(key => (<Table.Cell>{item[key]}</Table.Cell>))}
            </Table.Row>
        )))
    }, [checkedHeader, sortedFilterList])

    function handlePageChange(e, { activePage }) {
        
        setiActivePage(iActivePage);
        setbegin(activePage * 5 - 5);
        setend(activePage * 5);

     //   filteredList = _.sortBy(filteredList, [column]);
     setFilteredList(_.sortBy(filteredList, [column]))  
     //if (direction === "descending") filteredList = filteredList.reverse();
        setSortedFilterList(filteredList.slice(activePage * 5 - 5, activePage * 5));
        console.log(activePage,"form handle change active page")
    }

    const handleSort = (clickedColumn) => () => {
        if (column !== clickedColumn) {
            console.log("Clicked Column name if", clickedColumn)

            setcolumn(clickedColumn);
           // filteredList = _.sortBy(filteredList, [clickedColumn]);
     setFilteredList(_.sortBy(filteredList, [clickedColumn]))  
           
           setdirection("ascending");
        } else {
            console.log("Clicked Column name else", clickedColumn)
            setFilteredList(filteredList.reverse())  

         //   filteredList = filteredList.reverse();
            setdirection(direction === "ascending" ? "descending" : "ascending");
        }
        setSortedFilterList(filteredList.slice(begin, end));
    };
    function dateFormatter(date) {
        return date != null ? moment(date).format("DD-MM-YYYY") : ""; // new Date(data).toLocaleString('en-AU') : '';
    }

    return (
        <div style={{ width: "100%" }}>
            {/* <div style={{ maxwidth: "100%", overflow: "auto", gridAutoFlow: "scroll" }}> */}
            {sortedFilterList === undefined || sortedFilterList.length === 0 ? (
                <Header>No active job registration found</Header>
            ) : (
                <div>
                    <div style={{ maxwidth: "100%", overflow: "auto" }}>
                        <Table selectable celled sortable padded color='blue' >
                            <Table.Header>
                                <Table.Row>
                                    {tableHeader}
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                {tableBody}
                            </Table.Body>
                        </Table>
                    </div>
                    <br />
                    <div>
                        <Menu floated="right" pagination>
                            <Menu.Item>
                                <Pagination
                                    defaultActivePage={1}
                                    totalPages={Math.ceil(filteredList.length / 5)}
                                    onPageChange={handlePageChange}
                                />
                            </Menu.Item>
                        </Menu>
                    </div>
                </div>
            )}
        </div>
    )
}

export default RecruitmentGridAll;