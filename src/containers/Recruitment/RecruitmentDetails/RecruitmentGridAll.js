import React, { useState, useEffect } from "react";
import { Icon, Menu, Table, Header, Pagination, TableHeader, TableBody, Dropdown, Step } from "semantic-ui-react";
import _ from "lodash";
import moment from "moment";
import { data } from '../../../Data/RecruitmentList';

let filteredList = [];

const RecruitmentGridAll = (props) => {
    //console.log(props,"from recruitmentall")
    const headers = props.headers;
    const items = props.items;

    const [begin, setBegin] = useState(0);
    const [end, setEnd] = useState(5);
    const [sortedFilterList, setSortedFilterList] = useState(items.slice(begin, end));
    const [iActivePage, setiActivePage] = useState(1);
    const [column, setcolumn] = useState(null);
    const [direction, setdirection] = useState("ascending");
    const [checkedHeader, setCheckedHeader] = useState([]);
    const [tableHeader, setTableHeader] = useState([]);
    const [tableBody, setTableBody] = useState([])
    const [rowSize, setRowSize] = useState(5);

    const options = [
        { key: 1, text: '5 rows', value: 5 },
        { key: 2, text: '10 rows', value: 10 },
        { key: 3, text: '20 rows', value: 20 },
    ]
    const totalPages = items.length;

    useEffect(() => {
        filteredList = items;
        console.log(" items in begining ", items)
        console.log(" Filter list length ", filteredList.length, "  hello ", Math.ceil(filteredList.length / 5));
        if (filteredList !== undefined && filteredList.length > 0) {
            setSortedFilterList(filteredList.slice(begin, end))
        }
    }, []);


    useEffect(() => {
        filteredList = items;
        if (filteredList !== undefined && filteredList.length > 0) {
            setSortedFilterList(filteredList.slice(begin, end));
        }
    }, [begin, end]);


    useEffect(() => {
        console.log("Sorted Filtered List ", sortedFilterList, " Items ", items)
        // let listWithFilteredHeader = HeaderParser(sortedFilterList, checkedHeader);
        // let filteredHeader = Object.keys(listWithFilteredHeader[0]).map(key => key);
        setTableHeader(() => headers.map((header) => (
            <Table.HeaderCell
                sorted={column === header.Header ? direction : null}
                onClick={handleSort(header.Header)}
            >
                {header.Header}
            </Table.HeaderCell>
        ))
        )

        setTableBody(() => sortedFilterList.map((item) => (
            <Table.Row key={item.LeadSeqNo} onClick={handleBodyClicked(item)}>
                {headers.map((header) => (
                    <Table.Cell>
                        {item[header.Header]}
                    </Table.Cell>
                ))}
            </Table.Row>
        ))
        )
    }, [sortedFilterList, headers])

    const handleBodyClicked = (item) => {
        console.log(item);
    }

    function handlePageChange(e, { activePage }) {
        setiActivePage(iActivePage);
        setBegin(activePage * rowSize - rowSize);
        setEnd(activePage * rowSize);

        filteredList = _.sortBy(filteredList, [column]);
        if (direction === "descending") filteredList = filteredList.reverse();
        setSortedFilterList(filteredList.slice(activePage * rowSize - rowSize, activePage * rowSize));
        // setSortedFilterList(filteredList.slice(begin,end));

        console.log(activePage, "from handle change active page")
    }

    const handlePageSelection = (e, { value }) => {
        setRowSize(value);
        setBegin(0);
        setEnd(value);
    }

    const handleSort = (clickedColumn) => () => {
        if (column !== clickedColumn) {
            console.log("Clicked Column name if", clickedColumn)
            setcolumn(clickedColumn);
            filteredList = _.sortBy(filteredList, [clickedColumn]);
            setdirection("ascending");
        } else {
            console.log("Clicked Column name else", clickedColumn)
            filteredList = filteredList.reverse();
            setdirection(direction === "ascending" ? "descending" : "ascending");
        }
        setSortedFilterList(filteredList.slice(begin, end));
    };

    // function dateFormatter(date) {
    //     return date != null ? moment(date).format("DD-MM-YYYY") : ""; // new Date(data).toLocaleString('en-AU') : '';
    // }

    return (
        <div style={{ width: "100%" }}>
            {/* <div style={{ maxwidth: "100%", overflow: "auto", gridAutoFlow: "scroll" }}> */}
            {sortedFilterList === undefined || sortedFilterList.length === 0 ? (
                <Header>No active job registration found</Header>
            ) : (
                <div>
                    <div style={{ maxwidth: "100%", overflow: "auto" }}>
                        <Table selectable celled sortable padded color='blue' >
                            <Table.Header>
                                <Table.Row>
                                    {tableHeader}
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                {tableBody}
                            </Table.Body>
                        </Table>
                    </div>
                    <br />
                    <div>
                        <Menu floated="right" pagination>
                            <Menu.Item>
                                <Dropdown onChange={handlePageSelection} defaultValue={5} options={options} />
                            </Menu.Item>
                            <Menu.Item>
                                <Pagination
                                    defaultActivePage={1}
                                    totalPages={Math.ceil(totalPages / rowSize)}
                                    onPageChange={handlePageChange}
                                />
                            </Menu.Item>
                        </Menu>
                    </div>
                </div>
            )}
        </div>
    )
}

export default RecruitmentGridAll;