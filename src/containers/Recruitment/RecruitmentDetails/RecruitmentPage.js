import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Grid } from "semantic-ui-react";
import Navbar from "../../../components/layout/Navbar";
import { data } from "../../../Data/RecruitmentList";
import DropdownComponent from "./DropdownComponent";
import RecruitmentGridAll from "./RecruitmentGridAll";
import RecruitmentModal from "./RecruitmentModal";



const RecruitmentPage = () => {
    const headers = useSelector((state) => state.headers);
    console.log(headers, "form App.js")
    let initialHeaders = headers.map((header) => header.innerHeader.filter((head) => head.checked))
        .flat();
    const [checkedHeader, setCheckedHeader] = useState(initialHeaders);


    useEffect(() => {
        setCheckedHeader(
            headers.map((header) => header.innerHeader.filter((head) => head.checked))
                .flat()
        );
    }, [headers]);
    console.log(headers, "header form RecruitmentPage");
    console.log(checkedHeader, "checkedHeader form RecruitmentPage");

    useEffect(() => { }, [headers]);

    return (
        <Grid divided="vertically" style={{ padding: "1.5rem" }} >
            <Grid.Row >
                <Navbar />
            </Grid.Row>
            <Grid.Row >
                <RecruitmentModal headers={checkedHeader} />
                <DropdownComponent headers={headers} />
            </Grid.Row>
            <Grid.Row>
                <RecruitmentGridAll headers={checkedHeader} items={data} />
            </Grid.Row>
        </Grid>
    )
}

export default RecruitmentPage;
