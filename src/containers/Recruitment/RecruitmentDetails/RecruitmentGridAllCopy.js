import React, { useState, useEffect } from "react";
import { Icon, Menu, Table, Header, Pagination } from "semantic-ui-react";
import _ from "lodash";
import moment from "moment";
import { data } from '../../../Data/RecruitmentList';
import RecruitmentModal from "./RecruitmentModal";


let filteredList;

const RecruitmentGridAllCopy = (props) => {
    const [begin, setbegin] = useState(0);
    const [end, setend] = useState(5);
    const [items, setItems] = useState(data)
    const [sortedFilterList, setSortedFilterList] = useState([]);
    const [iActivePage, setiActivePage] = useState(1);
    const [column, setcolumn] = useState(null);
    const [direction, setdirection] = useState("ascending");
    console.log(props,"form Recruitmentgridall")


    useEffect(() => {
        filteredList = items;
        if (filteredList !== undefined && filteredList.length > 0) {
            setSortedFilterList(filteredList.slice(begin, end))
        }
    }, [])

    useEffect(() => {
        filteredList = items;
        if (filteredList !== undefined && filteredList.length > 0) {
            setSortedFilterList(filteredList.slice(begin, end));
        }
    }, [items]);
    function handlePageChange(e, { activePage }) {
        setiActivePage(iActivePage);
        setbegin(activePage * 5 - 5);
        setend(activePage * 5);

        filteredList = _.sortBy(filteredList, [column]);
        if (direction === "descending") filteredList = filteredList.reverse();
        setSortedFilterList(filteredList.slice(activePage * 5 - 5, activePage * 5));
    }

    const handleSort = (clickedColumn) => () => {
        if (column !== clickedColumn) {
            setcolumn(clickedColumn);
            filteredList = _.sortBy(filteredList, [clickedColumn]);
            setdirection("ascending");
        } else {
            filteredList = filteredList.reverse();
            setdirection(direction === "ascending" ? "descending" : "ascending");
        }
        setSortedFilterList(filteredList.slice(begin, end));
    };
    function dateFormatter(date) {
        return date != null ? moment(date).format("DD-MM-YYYY") : ""; // new Date(data).toLocaleString('en-AU') : '';
    }

    return (
        <div>
            <RecruitmentModal/>
            {sortedFilterList === undefined || sortedFilterList.length === 0 ? (
                <Header>No active job registration found</Header>
            ) : (
                <Table selectable celled sortable>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell
                                sorted={column === "Consultant" ? direction : null}
                                onClick={handleSort("Consultant")}
                            >
                                Consultant
                            </Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={column === "LeadSeqNo" ? direction : null}
                                onClick={handleSort("LeadSeqNo")}
                            >
                                Lead No
                            </Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={column === "ContactName" ? direction : null}
                                onClick={handleSort("ContactName")}
                            >
                                Contact
                            </Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={column === "AreaOfInterest" ? direction : null}
                                onClick={handleSort("AreaOfInterest")}
                            >
                                Area of Interest
                            </Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={column === "State" ? direction : null}
                                onClick={handleSort("State")}
                            >
                                State
                            </Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={column === "LeadStatus" ? direction : null}
                                onClick={handleSort("LeadStatus")}
                            >
                                Lead Status
                            </Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={column === "Progress" ? direction : null}
                                onClick={handleSort("Progress")}
                            >
                                Progress
                            </Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={column === "InitialContact" ? direction : null}
                                onClick={handleSort("InitialContact")}
                            >
                                Initial Contact
                            </Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={column === "InfoSession" ? direction : null}
                                onClick={handleSort("InfoSession")}
                            >
                                Info Session
                            </Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={column === "FinalInterview" ? direction : null}
                                onClick={handleSort("FinalInterview")}
                            >
                                Final Interview
                            </Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={column === "FranchiseeAgreementSent" ? direction : null}
                                onClick={handleSort("FranchiseeAgreementSent")}
                            >
                                Agreement Sent
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {sortedFilterList.map((item) => (
                            <Table.Row key={item.LeadSeqNo}>
                                <Table.Cell key={item.LeadSeqNo}>
                                    {item.Consultant}
                                </Table.Cell>
                                <Table.Cell key={item.LeadSeqNo}>
                                    {item.LeadSeqNo}
                                </Table.Cell>
                                <Table.Cell key={item.LeadSeqNo}>
                                    {item.ContactName}
                                </Table.Cell>
                                <Table.Cell key={item.LeadSeqNo}>
                                    {item.AreaOfInterest}
                                </Table.Cell>
                                <Table.Cell key={item.LeadSeqNo}>
                                    {item.State}
                                </Table.Cell>
                                <Table.Cell key={item.LeadSeqNo}>
                                    {item.LeadStatus}
                                </Table.Cell>
                                <Table.Cell key={item.LeadSeqNo}>
                                    {item.Progress}
                                </Table.Cell>
                                <Table.Cell key={item.LeadSeqNo}>
                                    {item.InitialContact}
                                </Table.Cell>
                                <Table.Cell key={item.LeadSeqNo}>
                                    {item.InfoSession}
                                </Table.Cell>
                                <Table.Cell key={item.LeadSeqNo}>
                                    {item.FinalInterview}
                                </Table.Cell>
                                <Table.Cell key={item.LeadSeqNo}>
                                    {item.FranchiseeAgreementSent}
                                </Table.Cell>
                            </Table.Row>
                        ))}
                    </Table.Body>
                    <Table.Footer>
                        <Table.Row>
                            <Table.HeaderCell colSpan="15">
                                <Menu floated="right" pagination>
                                    <Pagination
                                        defaultActivePage={1}
                                        // totalPages={Math.ceil(filteredList.length / 5)}
                                        onPageChange={handlePageChange}
                                    />
                                </Menu>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>

                </Table>
            )}
        </div>
    )
}

export default RecruitmentGridAllCopy;