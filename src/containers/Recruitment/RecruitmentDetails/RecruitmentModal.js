import React, { useEffect, useState } from 'react';
import { Button, Checkbox, Dropdown, Header, Icon, Modal } from 'semantic-ui-react'
import CheckboxComponent from './CheckBox';


const RecruitmentModal = ({ headers }) => {
    const [open, setOpen] = React.useState(false)

    useEffect(() => { }, [open])

    return (
        <Modal
            onClose={() => setOpen(false)}
            onOpen={() => setOpen(true)}
            open={open}
            size='small'
            trigger={<Button color='blue'>Basic Modal <Dropdown/></Button>}
        >
            <Header icon>
                <Icon name='archive' />
                Different Fields
            </Header>
            <Modal.Content>
                <CheckboxComponent headers={headers} />
            </Modal.Content>
            <Modal.Actions>
                <Button color='green' inverted onClick={() => setOpen(false)}>
                    <Icon name='checkmark' /> View
                </Button>
            </Modal.Actions>
        </Modal>
    )
}

export default RecruitmentModal;
