import React, { useEffect, useState } from "react";
import { UPDATE_HEADERS } from "../../../Actions/type"
import { useDispatch } from "react-redux";
import { Checkbox, Grid, Header } from "semantic-ui-react";

const ChangeChildHeader = (headers, childHeader) => {
    return headers.map((header) =>
        header.innerHeader.findIndex((innerHead) => innerHead.Header === childHeader) >= 0 ? {
            ...header, innerHeader: header.innerHeader.map((insideHeader) =>
                insideHeader.Header === childHeader ? { ...insideHeader, checked: !insideHeader.checked } : insideHeader)
        } : header
    )
}

const ChangeParentHeader = (headers, parentHeaders) => {
    const flag = headers.find((header) => header.Type === parentHeaders).checked;

    return headers.map((parentHeader) =>
        parentHeader.Type === parentHeaders ? {
            ...parentHeader, checked: !flag, innerHeader: parentHeader.innerHeader.map((childHeader) => {

                return { ...childHeader, checked: !flag };
            })
        } : parentHeader
    )
}


const DropdownModal = (props) => {

    const [headers, setHeaders] = useState(props.headers);
    const [parentHeader, setParentHeader] = useState();
    const dispatch = useDispatch();

    useEffect(() => {
        setParentHeader(() => headers.map((header) => header.Type));
    }, []);

    useEffect(() => {
        dispatch({ type: UPDATE_HEADERS, payload: headers });
    }, [headers]);

    const handleCheckboxChange = (e, { value }) => {
        e.preventDefault();
        if (parentHeader.includes(value)) {
            setHeaders(ChangeParentHeader(headers, value));
        } else {
            setHeaders(ChangeChildHeader(headers, value));
        }
    }

    return (
        <Grid columns={headers.length} divided style={{ padding: "2px" }}>
            {headers !== undefined ? (
                headers.map((header) => (
                    <Grid.Column>
                        <Grid.Row>
                            <Header as="h4">
                                <Checkbox
                                    label={header.Type}
                                    value={header.Type}
                                    onChange={handleCheckboxChange}
                                />
                            </Header>
                        </Grid.Row>
                        {header.innerHeader.map((head) => (
                            <Grid.Row style={{ padding: "3px" }} verticalAlign="bottom">
                                <Checkbox
                                    label={head.Header}
                                    value={head.Header}
                                    checked={head.checked}
                                    onChange={handleCheckboxChange}
                                />
                            </Grid.Row>
                        ))}
                    </Grid.Column>
                )
                )
            ) : (<></>)}
        </Grid>
    )
}

export default DropdownModal;
