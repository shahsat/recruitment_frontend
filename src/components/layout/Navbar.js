import React from "react";
import { Button, Grid, Header, Icon, Segment } from "semantic-ui-react";
import HHlogo from '../../../src/icon/hh_logo.png'

const Navbar = () => {
    const h360URL = process.env.REACT_APP_H360_URL;
    return (
        <div style={{ width: "100%" }}>
            <Segment inverted color='blue'>
                <Grid columns="equal">
                    <Grid.Column floated="left" verticalAlign="middle" width={2}>
                        {/* <Menu.Item> */}
                        <img id="hhlogo" src={HHlogo} height="20px" width="auto" />
                        {/* </Menu.Item> */}
                    </Grid.Column>
                    <Grid.Column width={5} floated="right">
                        {/* <Menu.Item> */}
                        <Header as="h2" floated="right">Recruitment Status</Header>
                    </Grid.Column>
                    <Grid.Column width={5} verticalAlign="middle">
                        {/* {currentuser !== undefined && currentuser.username !== "" ? (
            <Label color="grey" basic image>
              {currentuser.role === "Franchisee" ? (
                <img src={currentuser.linkedFranchisees[0].ImgFileName} />
              ) : (
                <Icon name="user circle" />
              )}
              {currentuser.username}
              <Label.Detail>
                {currentuser.linkedFranchisees[0] !== undefined &&
                  currentuser.role === "Franchisee"
                  ? currentuser.linkedFranchisees[0].FN +
                  " - " +
                  currentuser.linkedFranchisees[0].company
                  : "Support Office"}
              </Label.Detail>
            </Label>
          ) : null} */}
                        <p>Current User</p>
                    </Grid.Column>
                    <Grid.Column floated="right" width={2}>
                        <Button circular>
                            <a href={""} target="_blank" rel="noopener noreferrer">
                                <Icon name="book" /> Recruitment Manual
                            </a>
                        </Button>
                    </Grid.Column>
                    <Grid.Column floated="right" width={2}>
                        <Button
                            content="H360"
                            icon="left arrow"
                            href={h360URL}
                            labelPosition="left"
                        />
                    </Grid.Column>
                </Grid>
                {/* </Menu> */}
                {/* </Container> */}
            </Segment>
        </div>
    )
}

export default Navbar;
